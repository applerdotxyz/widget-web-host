
const widgetRootTag = document.getElementById('root');

if(widgetRootTag === null){
    const rootElement = document.createElement('div');
    rootElement.id = 'root';
    document.body.appendChild(rootElement);
}


let s1 = document.createElement('script');
s1.setAttribute('src', 'https://9to5-widget.vercel.app/static/js/2.0311e476.chunk.js')

let s2 = document.createElement('script');
s2.setAttribute('src', 'https://9to5-widget.vercel.app/static/js/app.3741619f.chunk.js')

let s3 = document.createElement('script');
s3.setAttribute('src', 'https://9to5-widget.vercel.app/static/js/runtime~app.2e9f1821.js')

document.body.appendChild(s1);
document.body.appendChild(s2);
document.body.appendChild(s3);