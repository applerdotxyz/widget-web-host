# widget-web-host

URL: https://widget-web-host.vercel.app/

# How to embed 

- Create one div element with id="root" in your application. 
``` 
<div id="root"></div>
```
- Add the script tag with provided src.
- Give necessary styles to #root according to your requirement but `display: flex` is required.
- Add the script tag to every page, where you want the widget to appear.

## For React applications

- Follow the same steps as above, you just need to embed script in public/index.html.

# Things to remember (For us as Dev)

- Provided src should contain other scripts and need to be embedded dynamically.
- Verify the user before serving the script. (TBD later)

## Challenges 

- Make the app working on some other root element other than (div#root), because React applications by default serve their content on #root element.
- Make the button appear widget-content only when clicked.